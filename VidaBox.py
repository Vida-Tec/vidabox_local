#!/usr/bin/env python3

import signal,MFRC522,VB,subprocess
from time import sleep

MIFAREReader = ''
playerState = 'init'
continue_reading = True
chipsArray = ''
nowPlay = ''

def end_read(signal, frame):
    global continue_reading
    continue_reading = False
    GPIO.cleanup()

def readyBox():
	VB.LoadVidaBoxConfig()
	global chipsArray
	global MIFAREReader
	signal.signal(signal.SIGINT, end_read)
	MIFAREReader = MFRC522.MFRC522()
	chipsArray = VB.getChipsArray()
	VB.LED.greenON()
	VB.startUpSound()

readyBox()

while continue_reading:
	
    (status, TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)	
	
    if status == MIFAREReader.MI_OK:
		VB.checkShutdownTime()
		VB.LED.colorON()
		(status, uid) = MIFAREReader.MFRC522_SelectTagSN()
		if status == MIFAREReader.MI_OK:
			audioTitel = VB.getValueByName(VB.uidToString(uid),chipsArray)[2]
			count = audioTitel.count('.mp3')
			if count > 0:
				if playerState != audioTitel:
					if playerState == 'pause' and nowPlay == audioTitel:
						VB.LED.colorON()
						playerState = audioTitel
						nowPlay = audioTitel
						VB.pygame.mixer.music.play()
					else:
						chipsArray = (VB.getChipsArray())
						VB.LED.startNewFigur()
						VB.playAudio(str(audioTitel))
						playerState = audioTitel
			else:
				if chipsArray[0][0] == 'error':
					VB.LED.error(3)
				else:	
					VB.LED.error(5)	
		else:
			playerState = VB.pauseMedia(audioTitel)
			nowPlay = audioTitel
	