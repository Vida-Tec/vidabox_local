#!/usr/bin/env python3

import signal,LED,MFRC522,requests,os,subprocess,ast,serial
from time import sleep
from time import gmtime, strftime
import pygame

mediaLink = ''
startSound = ''
shutdownTime = ''
playOnFigur = ''
	
def getBoxConfigFromDb():
	file = open("/home/pi/vidabox/setup.TXT", "r")
	if file.mode == 'r':
		configFile = file.read()
		dbConfigDataArray = ast.literal_eval(configFile)
				
	return dbConfigDataArray
	
def getChipconfigFromDb():
	file = open("/home/pi/vidabox/conf.TXT", "r")
	if file.mode == 'r':
		configFile = file.read()
		chipConfig = ast.literal_eval(configFile)
	else:
		chipConfig = {'name': 'demo', 'password': 'demo', 'version': '2'}
	return chipConfig
	
def getChipsFromServer():
	chipConfig = getChipconfigFromDb()
	r = requests.get('http://vida-tec.de/get/userData.php', headers={'X-Platform': 'VidaBox'}, params={'version': chipConfig['version'],'name': chipConfig['name'],'password': chipConfig['password']})
	r.headers
	
	return (r.text)

def getChipsArray():
	file = open("/home/pi/vidabox/RFIDList.TXT", "r")
	if file.mode == 'r':
		configFile = file.read()
		chipConfig = ast.literal_eval(configFile)
			
	return chipConfig
	
def getValueByName(name,chipsArray):
	for value in chipsArray:
		if str(value[0]) == str(name):
			return(value)
	return 'null'
	
def uidToString(uid):
    uidString = ""
    for i in uid:
        uidString = format(i, '02X') + uidString
    return uidString

def playAudio(url):
	mediaPath = mediaLink+str(url)
	pygame.mixer.init()
	if mediaLink.find('http') != -1:
		r = requests.get(mediaPath,stream=True)
		pygame.mixer.music.load(r.raw)
	else:
		pygame.mixer.music.load(mediaPath)
	pygame.mixer.music.play()

def startUpSound():
	if startSound.count('.mp3'):
		playAudio(startSound)

def checkShutdownTime():
	if shutdownTime != '0':	
		timeNow = str(subprocess.check_output(["sudo","date","+%H:%M"]).split('\n')[0])
		if str(shutdownTime) == timeNow:
			print('DOWN')
			down = subprocess.check_output(["sudo","shutdown","-h","now"])
def pauseMedia(playerState):
	if playOnFigur == '1':
		LED.greenON()
		pygame.mixer.music.pause()
		return 'pause'
	else:
		return playerState
		
def LoadVidaBoxConfig():
	global mediaLink
	global startSound
	global shutdownTime
	global playOnFigur
	BoxConfig = getBoxConfigFromDb()
	mediaLink = BoxConfig[0]
	startSound = BoxConfig[1]
	shutdownTime = BoxConfig[2]
	playOnFigur = BoxConfig[3]
	

